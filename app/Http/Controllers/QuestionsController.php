<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class QuestionsController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'title' => 'required|max:45',
            'body' => 'required|max:255',
        ]);

        $query = DB::table('pertanyaan')->insert([
            'judul' => $request['title'],
            'isi' => $request['body']
        ]);

        return redirect('/pertanyaan')->with('success', 'Question saved!');
    }

    public function index() {
        $questions = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', ['questions' => $questions]);
    }

    public function show($pertanyaan_id) {
        $question = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.detail', ['question' => $question]);
    }

    public function edit($pertanyaan_id) {
        $question = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.edit', ['question' => $question]);
    }

    public function update(Request $request, $pertanyaan_id) {
        $validatedData = $request->validate([
            'title' => 'required|max:45',
            'body' => 'required|max:255',
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $pertanyaan_id)
                    ->update([
                        'judul' => $request['title'],
                        'isi' => $request['body']
                    ]);

        return redirect('/pertanyaan')->with('success', 'Question edited!');
    }

    public function destroy($pertanyaan_id) {
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();

        return redirect('/pertanyaan')->with('success', 'Question deleted!');
    }
}
