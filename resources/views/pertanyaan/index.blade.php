@extends('layouts.master')

@section('title', 'Question list')

@section('content')
    <div class="card m-3">
        <div class="card-header">
        <h3 class="card-title">Question list</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <a href="/pertanyaan/create" class="btn btn-primary btn-sm">Create new question</a>
        <table class="table table-bordered mt-3">
            <thead>                  
            <tr>
                <th style="width: 10px">#</th>
                <th>Title</th>
                <th>Question</th>
                <th style="width: 20%">Action</th>
            </tr>
            </thead>
            <tbody>
            @forelse($questions as $key => $question)
                <tr>
                    <td>{{ $key + 1 }}.</td>
                    <td>{{ $question->judul }}</td>
                    <td>{{ $question->isi }}</td>
                    <td style="display: flex; justify-content: space-evenly">
                        <a href="/pertanyaan/{{ $question->id }}" class="btn btn-sm btn-info">Detail</a>
                        <a href="/pertanyaan/{{ $question->id }}/edit" class="btn btn-sm btn-warning">Edit</a>
                        <form action="/pertanyaan/{{ $question->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" align="center">Nothing question</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection