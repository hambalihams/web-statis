@extends('layouts.master')

@section('title', 'Create Question')

@section('content')
    <div class="card card-primary m-3">
        <div class="card-header">
        <h3 class="card-title">Create Question</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
         @csrf
        <div class="card-body">
            <div class="form-group">
            <label for="judul">Title</label>
            <input type="text" class="form-control" id="judul" name="title" value="{{ old('title', '') }}" placeholder="Insert title">
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
            <label for="isi">Question</label>
            <textarea id="isi" class="form-control" rows="3" name="body" placeholder="Insert question">{{ old('body', '') }}</textarea>
            @error('body')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        </form>
    </div>
@endsection