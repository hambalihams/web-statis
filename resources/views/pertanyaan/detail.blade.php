@extends('layouts.master')

@section('title', 'Detail page')

@section('content')    
    <div class="card card-primary m-3">
        <div class="card-header">
        <h3 class="card-title">Detail Question</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item">
                    <h3>Title:</h3>
                    <h4>{{ $question->judul }}</h4>
                </li>
                <li class="list-group-item">
                    <p>Body:</p>
                    <p>{{ $question->isi }}</p>
                </li>
            </ul>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <a href="/pertanyaan" class="btn btn-primary">Back</a>
        </div>
    </div> 
@endsection