@extends('layouts.master')

@section('title', 'Edit Question')

@section('content')
    <div class="card card-primary m-3">
        <div class="card-header">
        <h3 class="card-title">Edit Question</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan/{{ $question->id }}" method="POST">
         @csrf
         @method('PUT')
        <div class="card-body">
            <div class="form-group">
            <label for="judul">Title</label>
            <input type="text" class="form-control" id="judul" name="title" value="{{ old('title', $question->judul) }}" placeholder="Insert title">
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
            <label for="isi">Question</label>
            <textarea id="isi" class="form-control" rows="3" name="body" placeholder="Insert question">{{ old('body', $question->isi) }}</textarea>
            @error('body')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/pertanyaan" class="btn btn-secondary">Cancel</a>
        </div>
        </form>
    </div>
@endsection